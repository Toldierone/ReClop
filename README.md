# ReClop
  
## Branches of project:  
* vanilla  
 source code of >CLOP provided as-is (unchanged)  
* de-compound   
 >CLOP without few connections with Compounds database  
* de-tax  
 de-compounded >CLOP with taxes removed  
* auto-donor  
 free wings for everyone!  
* **release**  
 contains version of software that's currently hosted on [reclop.tk](http://reclop.tk)